#!/bin/bash

## Ce script automatise toutes les installations et la configuration après installation de la distributions.
## Le coeur du script est composé de deux partie la première installe qui paquet logiciel et la seconde installe des thèmes,
## des extensions et configurations à la fois communs et spécifiques des différents environnement de bureau.

# Création des différents choix pour la suite

echo -n "Que voulez-vous continuer l'execution du script? ( oui ou non ) "
read reponse1

case "$reponse1" in

oui )  
# Installation update & logiciel du dépôt de base + des dépôts supplémentaires

echo "Mise à jour de la distribution et installation des paquets."

sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y && sudo apt full-upgrade -y

sudo apt install intel-microcode
sudo apt install amd64-microcode
sudo apt install mythes-fr myspell-fr
sudo apt install git
sudo apt install ffmpeg
sudo apt install curl
sudo apt install wget
sudo apt install ufw gufw
sudo apt install gparted
sudo apt install gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-plugins-good
sudo apt install ubuntu-restricted-extras
sudo apt install ghostwriter
sudo apt install default-jre default-jre-headless
sudo apt install keepassxc
sudo apt install lsb-base lsb-printing
sudo apt install libsane sane-utils xsltproc
sudo apt install nano
sudo apt install pandoc
sudo apt install lollypop
sudo apt install steam
sudo apt install simple-scan
sudo apt install scribus
sudo apt install texlive-full
sudo apt install thunderbird thunderbird-locale-fr
sudo apt install gtkhash
sudo apt install gimp gimp-help-fr
sudo apt install qpdfview qpdfview-ps-plugin
sudo apt install python3-pydrive deja-dup
sudo apt install timeshift
sudo apt install pdfarranger
sudo apt install gnome-maps
sudo apt install nmap
sudo apt install xclip
sudo apt install soundconverter
sudo apt install celluloid
sudo apt install imagemagick
sudo apt install gnome-system-monitor
sudo apt install skrooge
sudo apt install software-properties-common
sudo apt install deja-dup
sudo apt install fdupes
sudo apt install fdisk
sudo apt install wipe
sudo apt install scrub
sudo apt install partclone
sudo apt install inkscape

sudo apt install sassc gtk2-engines-murrine gtk2-engines-pixbuf
sudo adduser $USER lp
gufw
sudo systemctl --global disable redshift-gtk
sudo systemctl --global disable redshift
for p in libreoffice* mpv* ; do sudo apt autoclean $p && sudo apt autoremove $p ; done
snap remove --purge firefox
sudo apt install -y flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub net.cozic.joplin_desktop
flatpak install flathub com.spotify.Client
flatpak install flathub com.mojang.Minecraft
flatpak install flathub org.onlyoffice.desktopeditors
flatpak install flathub org.musicbrainz.Picard
flatpak install flathub com.github.johnfactotum.Foliate

# Ajout dépôt deb Firefox
# Pour plus d'information https://doc.ubuntu-fr.org/firefox
sudo add-apt-repository ppa:mozillateam/ppa
sudo cp mozillateamppa /etc/apt/preferences.d/mozillateamppa
sudo cp 51unattended-upgrades-firefox /etc/apt/apt.conf.d/51unattended-upgrades-firefox
sudo apt update
sudo apt install -y firefox firefox-locale-fr

# Ajout dépôt VSCodium
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium.gpg 
echo 'deb https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list 
sudo apt update && sudo apt install -y codium

    # Virtualbox

    echo -n "Voulez-vous installer VirtualBox ? ( oui ou non ) "
    read reponse2 

    case "$reponse2" in

    oui )
        wget -q -O- http://download.virtualbox.org/virtualbox/debian/oracle_vbox_2016.asc | sudo apt-key add -
        echo "deb http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" | sudo tee /etc/apt/sources.list.d/virtualbox.list
        sudo apt update
        sudo apt install virtualbox-6.1
    ;;
    non )
        echo "VirtualBox ne sera pas installé"
    ;;
    * )
        echo "Faite un choix valide "
    ;;
    esac

# Balena Etcher
curl -1sLf \
   'https://dl.cloudsmith.io/public/balena/etcher/setup.deb.sh' \
   | sudo -E bash
sudo apt update
sudo apt install balena-etcher-electron

# Dropbox
firefox https://www.dropbox.com/

# Alias bash personnalisé  

cp bash_aliases.txt ~/.bash_aliases

git clone https://framagit.org/G-Bas/fichier-de-configuration-linux.git
cd fichier-de-configuration-linux

# Couleurs personnalisées de ls
cp ls_colors.txt ~/.ls_colors
dircolors -p > ~/.ls_colors

# Modification du swappiness
sudo bash -c 'echo "vm.swappiness=10" >> /etc/sysctl.conf'
sudo sysctl -p

cd ..

# Nettoyage du dossier

wipe -frie -q fichier-de-configuration-linux

# Ajout de thèmes communs

    for d in icons themes ; do mkdir -p ~/.local/share/"$d" && ln -s ~/.local/share/"$d" ~/."$d" ; done

        # Thème pour GNOME et Budgie

        if [[ "$XDG_CURRENT_DESKTOP" = "ubuntu:GNOME" || "$XDG_CURRENT_DESKTOP" = "Budgie:GNOME" ]]; then

            git clone https://github.com/vinceliuice/Fluent-icon-theme.git
            cd Fluent-icon-theme/
            ./install.sh -d ~/.local/share/icons {standard,green,red,orange}
            cd ..

            git clone https://github.com/vinceliuice/Fluent-gtk-theme.git
            cd Fluent-gtk-theme/
            ./install.sh -d ~/.local/share/themes -s compact -t {default,green,red,orange} -c {dark,standard} -i ubuntu --tweaks round,square
            cd ..

            # Nettoyage de tous les dossiers

            for d in Fluent-gtk-theme Fluent-icon-theme ; do wipe -frie -q $d ; done

        fi

        if [[ "$XDG_CURRENT_DESKTOP" = "XFCE" || "$XDG_CURRENT_DESKTOP" = "Budgie:GNOME" ]]; then

            git clone https://github.com/vinceliuice/Colloid-gtk-theme.git
            cd Colloid-gtk-theme/
            ./install.sh -d ~/.local/share/themes -s compact -t {default,red,green,orange} --tweaks rimless normal
            cd ..

            wget -qO- https://git.io/papirus-icon-theme-install | DESTDIR="$HOME/.icons" sh

            wipe -frie -q Colloid-gtk-theme/

        fi

        if [[ "$XDG_CURRENT_DESKTOP" = "XFCE" || "$XDG_CURRENT_DESKTOP" = "ubuntu:GNOME" ]]; then

            git clone https://github.com/EliverLara/Nordic-Polar.git
            mv Nordic-Polar ~/.local/share/themes/Nordic-Polar

            git clone https://github.com/EliverLara/Kripton.git
            mv Kripton ~/.local/share/themes/Kripton

            git clone https://github.com/EliverLara/Juno.git
            mv Juno ~/.local/share/themes/Juno

        fi

        if [ " $XDG_CURRENT_DESKTOP" = "XFCE" ]; then

            git clone https://github.com/EliverLara/Ant-Nebula.git
            mv Ant-Nebula ~/.local/share/themes/Ant-Nebula

            git clone https://github.com/EliverLara/Ant.git
            mv Ant ~/.local/share/themes/Ant

            sudo apt install -y redshift redshift-gtk

        fi

        if [ "$XDG_CURRENT_DESKTOP" = "Budgie:GNOME" ]; then

            git clone https://github.com/vinceliuice/Jasper-gtk-theme.git
            cd Jasper-gtk-theme/
            ./install.sh -d ~/.local/share/themes -t {default,red,orange,blue,green} -s compact
            cd ..

            git clone https://github.com/vinceliuice/Orchis-theme.git
            cd Orchis-theme/
            ./install.sh -d ~/.local/share/themes -t {default,red,orange,green} -s compact --tweaks primary
            cd ..

            git clone https://github.com/oskonnikov/ArcBlackThemePantone.git
            cd ArcBlackThemePantone/
            
		    for t in Arc-Black*
		    do
    	    		if [ "$t" != "Arc-Black-Aspen-Gold" ] && [ "$t" != "Arc-Black-Pink-Peacock" ] && [ "$t" != "Arc-Black-Sweet-Liac" ]; then
            		mv $t ~/.local/share/themes/"$t"
    	    		fi
		    done
		
		    cd ..
	
		    git clone https://github.com/oskonnikov/FlatRemixDarkIconsPantone.git
		    cd FlatRemixDarkIconsPantone/

		    for t in Flat-Remix-Dark*
		    do
    	    		if [ "$t" != "Flat-Remix-Dark-Aspen-Gold" ] && [ "$t" != "Flat-Remix-Dark-Pink-Peacock" ] && [ "$t" != "Flat-Remix-Dark-Sweet-Liac" ]; then
            		mv $t ~/.local/share/icons/"$t"
    	    		fi
		    done
		
		    cd ..

            # Nettoyage de tous les dossiers

            for d in Jasper-gtk-theme Orchis-theme ArcBlackThemePantone FlatRemixDarkIconsPantone ; do wipe -frie -q "$d/" ; done

            sudo apt install -y budgie-weathershow-applet

        fi

        if [ "$XDG_CURRENT_DESKTOP" = "ubuntu:GNOME" ]; then

            # thème pour GNOME

            git clone https://github.com/vinceliuice/Graphite-gtk-theme.git
            cd Graphite-gtk-theme/
            ./install.sh -d ~/.local/share/themes -t {blue,red,green,orange,teal} -c {dark,standard} --tweaks normal
            cd ..

            git clone https://github.com/vinceliuice/Tela-circle-icon-theme.git
            cd Tela-circle-icon-theme/
            ./install.sh -d ~/.local/share/icons {blue,red,green,ubuntu,manjaro} -c
            cd

            # Nettoyage de tous les dossiers

            for d in Graphite-gtk-theme Tela-circle-icon-theme ; do wipe -frie -q "$d/" ; done

            # Ces paquets permettent la gestion des thèmes et extensions.
            sudo apt install gnome-shell-extensions chrome-gnome-shell gnome-tweaks
            sudo apt install gnome-shell-extension-manager
            sudo apt install gnome-themes-extra

            firefox https://extensions.gnome.org/extension/19/user-themes/
        
            git clone https://gitlab.com/skrewball/openweather.git
            cd openweather/
            make && make install
            cd ..
        
            git clone https://github.com/Deminder/ShutdownTimer.git
            cd ShutdownTimer/
            make install
            cd ..
        
            git clone https://github.com/stuarthayhurst/alphabetical-grid-extension.git
            cd alphabetical-grid-extension/
            make build & make install
            cd ..
        
            git clone https://github.com/Leleat/Tiling-Assistant.git
            cd Tiling-Assistant/
            bash scripts/build.sh -i
            cd ..
        
            git clone https://gitlab.com/paddatrapper/shortcuts-gnome-extension.git
            cd shortcuts-gnome-extension/
            ./install.sh
            cd ..

            git clone https://github.com/micheleg/dash-to-dock.git
            cd dash-to-dock/
            make
            make install
            cd ..

            # Nettoyage de tous les dossiers

            for d in openweather ShutdownTimer alphabetical-grid-extension Tiling-Assistant shortcuts-gnome-extension dash-to-dock Graphite-gtk-theme Tela-circle-icon-theme ; do wipe -frie -q "$d/" ; done
        
            #sudo firefox https://extensions.gnome.org/extension/4320/asusctl-gex/

            # Configuration de gnome
            sudo gsettings set org.gnome.desktop.peripherals.touchpad click-method 'areas'
            sudo gsettings reset org.gnome.shell app-picker-layout

        fi

    # Thème commun pour GNOME, XFCE et Budgie

    git clone https://github.com/vinceliuice/Qogir-theme.git
    cd Qogir-theme/
    ./install.sh -d ~/.local/share/themes -t {default,ubuntu,manjaro} -c {standard,dark} -i ubuntu --tweaks round
    cd ..
                                            
    git clone https://github.com/vinceliuice/Qogir-icon-theme.git
    cd Qogir-icon-theme/
    ./install.sh -d ~/.local/share/icons -t {default,ubuntu,manjaro} -c all
    cd ..

    wget -qO- https://git.io/papirus-icon-theme-install | DESTDIR="$HOME/.icons" sh

    # Nettoyage de tous les dossiers

    for d in Qogir-theme Qogir-icon-theme ; do wipe -frie -q $d ; done

        echo -n "Voulez-vous redémarrer l'ordinateur ? ( oui ou non ) "
        read reponse3 

        case "$reponse3" in

        oui )
        echo "L'ordinateur va redémarrer."
        sudo shutdown -t 10 -r now
        ;;
        non )
        echo "L'ordinateur restera actif.";;
        * )
        echo "Faite un choix valide ";;
        esac
;;
non )
echo "Le script n'executera pas aucune installation.";;
* )
echo "Faite un choix valide ";;
esac
