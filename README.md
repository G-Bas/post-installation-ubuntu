## Post-installation Ubuntu

Ce script automatise la mise à jour, l'installation des paquets et thèmes et ajout des fichiers de configuration après une installation fraîche de Ubuntu.
Celui-ci est l'original, qui peut être forké par n'importe qui. 

Pour executer le script, dan un terminal :

`git clone https://framagit.org/G-Bas/post-installation-ubuntu.git`

`cd post-installation-ubuntu`

`sudo sh post-installation-ubuntu.sh`